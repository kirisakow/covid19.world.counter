/**
 * URL query params:
 * - 'startDate', or 's': 'mm/dd/yyyy'-formatted date at which the countdown starts. 
 *   Default: '01/22/2020'.
 * - 'refreshRate', or 'r': time in milliseconds before transition to the next day
 *   Default: 500.
 * - 'daysPerSec', or 'dps': number of days per second to run. 
 *   Default: 2.
 */

/**
 * It all starts here.
 * Fetch the data
 */
const fetchData = (url) => {
  // download English locale
  d3.json("https://cdn.jsdelivr.net/npm/d3-time-format@2/locale/en-GB.json")
    .then((dataLocale, errorLocale) => {
      if (errorLocale) throw errorLocale;
      d3.timeFormatDefaultLocale(dataLocale);
      // download the COVID-19 data
      d3.csv(url)
        .then((dataCovid, errorCovid) => {
          if (errorCovid) throw errorCovid;
          const allDatesRecorded = [];
          var allNumsRecorded = [];
          // convert data structure from matrix to tree
          dataCovid =
            dataCovid.map((day) => {
              var obj = {};
              allDatesRecorded.push(day.date)
              obj["date"] = day.date;
              delete day.date;
              obj["data"] = Object.entries(day).map(([countryName, n]) => {
                n = +n
                allNumsRecorded.push(n);
                return { country: countryName, number: n };
              });
              return obj;
            });
          // the highest data figure
          const highestNumberInCsv = d3.max(allNumsRecorded);
          // if 'startDate' ('s') param is undefined, get the date from the first entry in the CSV file (and format it)
          startDateStr = getQueryParams('startDate') || getQueryParams('s') || d3.timeFormat('%m/%d/%Y')(Date.parse(dataCovid[0].date));
          drawViz(dataCovid, startDateStr, highestNumberInCsv, allDatesRecorded);
        });
    });
};
/**
 * Tokenize URL query params and return value to the 'key'
 */
const getQueryParams = (key) => {
  var params = new URL(window.location.href).searchParams;
  return params.get(key);
};
/**
 * Handy function 
 */
const isNullOrUndefined = (obj) => {
  return (null === obj || typeof obj === 'undefined');
};
/**
 * Format an MM/DD/YYYY string as are dates in CSV file: (M)M∕(D)D∕YY
 * E.g.: '02/01/2020' -> '2/1/20'
 */
const formatDateAsInCsv = (dateStr) => {
  return dateStr
    .replace(/^0/, '')
    .replace(/\/0/, '/')
    .replace(/([0-9][0-9])([0-9][0-9])/, '$2');
};
/**
 * Wipe what needs to be re-drawn
 */
const wipe = () => {
  // wipe header area
  d3.select('div#header').html('');
  // wipe chart area
  d3.select('#viz').html('');
};
/**
 * Recursive function which re-draws the dataviz
 * as many times as there are entries in the CSV file
 */
const drawViz = (data, dateStrMMDDYYYY, highestNumberInCsv, allDatesRecorded) => {

  // Set up the environment
  var margin =
    !isNullOrUndefined(margin)
      ? margin
      // approximate values
      : { left: 230, top: 10, right: 100, bottom: 10 };
  var width =
    !isNullOrUndefined(width)
      ? width
      // approximate values
      : window.innerWidth * 0.95;
  var height =
    !isNullOrUndefined(height)
      ? height
      // approximate values
      : 5000;
  var svg =
    d3.select('#viz')
      .append('svg')
      .attr('style', `width:${width}px; height:${height}px; background-color: #ffffff; border:2px lightgray solid;`)
      .append('g')
      .attr('transform', `translate(${margin.left}, ${margin.top})`);

  // Title
  var titleDayFormatted = d3.timeFormat('%A')(Date.parse(dateStrMMDDYYYY));
  var titleDateFormatted = d3.timeFormat('%d %B %Y')(Date.parse(dateStrMMDDYYYY));
  d3.select('div#header')
    .append('h1')
    .html(`Number of confirmed COVID-19 cases, by country, as of<br/>${titleDayFormatted}<br/>${titleDateFormatted}`);
  d3.select('div#header')
    .append('h5')
    .html(`<a href="https://gitlab.com/kirisakow/covid19.world.counter" target="_blank">Source code</a> | Powered by D3.js v. ${d3.version}`);

  // Data-related variables
  // format dateStrMMDDYYYY as in CSV file
  var dateStrMMDDYYYYAsInCsv = formatDateAsInCsv(dateStrMMDDYYYY);
  // sort countries as per number of COVID-19 cases, ascending
  // (countries have to be in ascending order so that they render in descending order... trust me)
  var currentDay = data.filter(day => day.date === dateStrMMDDYYYYAsInCsv)[0];
  currentDay.data = currentDay.data.sort((a, b) => a.number - b.number);

  // D3.js entrails
  // define scale for the axes
  var xScale =
    d3.scaleLinear()
      .range([0, width - margin.left - margin.right])
      .domain([0, highestNumberInCsv]);
  var yScale =
    d3.scaleBand()
      .range([height - margin.top - margin.bottom, 0])
      .padding(0.5)
      .domain(currentDay.data.map(d => d.country));

  // define y axis along which names of countries will show
  var yAxis =
    d3.axisLeft()
      .scale(yScale)
      .tickSize(0);
  //
  svg.append('g')
    .attr('class', 'y axis')
    .call(yAxis)
  //
  var bars =
    svg.selectAll('.bar')
      .data(currentDay.data)
      .enter()
      .append('g')
  //
  bars.append('rect')
    .attr('class', 'bar')
    .attr('y', (d) => yScale(d.country))
    .attr('height', yScale.bandwidth())
    // .attr('x', 0)
    .attr('width', (d) => xScale(d.number));
  //
  bars.append('text')
    .attr('class', 'label')
    // y axis labels (country names) will be aligned approximately in the middle relatively to each bar
    .attr('y', (d) => yScale(d.country) + yScale.bandwidth() / 2 + 4)
    // bar labels will show 3 pixels next to the bar on the right
    .attr('x', (d) => xScale(d.number) + 3)
    // format figures for better readbility: '100000' -> '100,000' -> '100 000'
    .text((d) => d3.format(',')(d.number).replace(/[,]/g, ' '));


  // Prepare the next iteration
  // next day's date
  var testDate = d3.timeDay.offset(Date.parse(dateStrMMDDYYYY), 1);
  // custom formatter
  var formatter = d3.timeFormat('%m/%d/%Y');
  // format testDate as in CSV file
  var testDateFormatted = formatDateAsInCsv(formatter(testDate));
  // if testDateFormatted is one of the dates in CSV file
  if (allDatesRecorded.includes(testDateFormatted)) {
    var dps = +getQueryParams('daysPerSec') || +getQueryParams('dps') || 2;
    var ms = +getQueryParams('refreshRate') || +getQueryParams('r') || (1000 / dps);
    // set timer so much millis
    setTimeout(function () {
      // Wipe what needs to be re-drawn
      wipe()
      // recursive call
      drawViz(data, testDateFormatted, highestNumberInCsv, allDatesRecorded);
    }, ms);
  }

};