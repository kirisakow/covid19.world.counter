
# Évolution du nombre de cas de COVID-19
Projet de data visualisation

Auteurs : Kiril Isakov, Salim Moubarik (M1 SIAD DS Classique, Univ Lille)

## Composition du projet

Ce projet consiste en deux volets :

* un tableau de bord réalisé en Shiny ;

* une visualisation animée réalisée en D3.js.

### Tableau de bord Shiny

#### Fonctionnement 

* Pour visualiser le tableau de bord Shiny, se rendre à [**http**://altdegauche.fr:3838/covid19.evolution/?**country**=Hong Kong](http://altdegauche.fr:3838/covid19.evolution/?c=Hong%20Kong), où le paramètre `country` (ou `c`) permet de choisir un pays. Veillez également à être en **HTTP** et non en **HTTPS**.
* Il suffit de sélectionner un pays dans la liste déroulante pour visualiser l'évolution des cas de COVID-19 sur son territoire.
* Pour générer la visualisation, l'application télécharge automatiquement les derniers chiffres officiels et les agrège en un graphique composé.

#### Fichiers

* open data : [derniers chiffres officiels](https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_time_series), consultables librement ;
* `app.R` avec le code de l'application ;
* `fonctions.R` avec quelques fonctions « maison » réutilisées à plusieurs reprises et sauvegardées à part pour éviter la duplication de code, comme le suggère la bonne pratique connue sous le nom « DRY ») ;

### Visualisation animée en D3.js

#### Fonctionnement

* Pour visualiser l'animation, se rendre à [https://altdegauche.fr/covid19.world.counter/?**startDate**=01/22/2020&**daysPerSec**=10](https://altdegauche.fr/covid19.world.counter/?startDate=01/22/2020&daysPerSec=10), où
    * le paramètre `daysPerSec` (ou `dps`) sert à définir la vitesse de l'animation ;
    * le paramètre `startDate` (ou `s`) sert à définir la date de départ (au format américain `MM/JJ/AAAA`).
* La visualisation se base sur un fichier CSV sauvegardé sur le même serveur que la visualisation elle-même. Ce fichier est créé à partir des données publiques, formatées à notre guise.
* Pour s'assurer que les données affichées dans l'animation sont à jour à tout moment, le fichier CSV est régulièrement régénéré à partir des dernières données publiées. Cela se fait au moyen d'un script R également présent sur le serveur qui s'occupe d'aller chercher les dernières données publiées, les formater et sauvegarder sur le serveur. Le script est déclenché automatiquement, une fois par heure, par un job `crontab`.

#### Fichiers

* open data : [derniers chiffres officiels](https://github.com/CSSEGISandData/COVID-19/tree/master/csse_covid_19_data/csse_covid_19_time_series), consultables librement ;
* un script R qui télécharge et transforme les données ;
* le code de l'application en JavaScript, HTML et CSS ;
* `fonctions.R` avec quelques fonctions « maison » réutilisées à plusieurs reprises et sauvegardées à part pour éviter la duplication de code, comme le suggère la bonne pratique connue sous le nom « DRY »).

